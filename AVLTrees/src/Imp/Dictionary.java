package Imp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import eg.edu.alexu.csd.filestructure.avl.IDictionary;

/**
 * As an application based on the AVL Tree implementation.
 * it is a simple English dictionary, supporting the following functionalities:
 * Load,insert,Delete,exist,size,height
 * @author Ahmed Maghawry
 *
 */
public class Dictionary implements IDictionary {
	/**
	 * A Tree object to use to imp the dictionary.
	 */
    private AVLTree tree;
    /**
     * Counter to count the size of the worlds.
     */
    private int counter = 0;

    /**
     * the constructor of the class.
     */
    public Dictionary() {
        tree = new AVLTree();
    }

    /**
     * the Load function to load a file from any dir and insert. 
     * the values in avl tree
     * @param file the file needed to read.
     */
    public final void load(final File file) {
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(file));
            String line = reader.readLine();
            while (line != null) {
                /*tree.insert(line);
                counter++;*/
            	insert(line);
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * insert a word to the dic.
     * @param word the word needed to be inserted.
     * @return a boolean indicates that success of the operation or not.
     */
    public final boolean insert(final String word) {
        if (exists(word)) {
            return false;
        }
        tree.insert(word);
        counter++;
        return true;
    }

    /**
     * search a word to the dic.
     * @param word the word needed to be searched.
     * @return a boolean indicates that success of the operation or not.
     */
    public final boolean exists(final String word) {
        return tree.search(word);
    }

    /**
     * delete a word to the dic.
     * @param word the word needed to be deleted.
     * @return a boolean indicates that success of the operation or not.
     */
    public final boolean delete(final String word) {
        if (tree.delete(word)) {
        	counter--;
        	update(counter);
            return true;
        }
        return false;
    }

	/**
     * number of words to the dic.
     * @return an Integer indicates that the number of words in the dictionary.
     */
    public final int size() {
        return counter;
    }

    /**
     * the height of the avl trees which contains the words.
     * @return the height of the tree. 
     */
    public final int height() {
        return tree.height();
    }

    /**
     * A function which prints the values in the dictionary Sorted.
     */
    public final void inOrder() {
        tree.inorder();
    }
    
    private void update(int counter2) {
		if (counter2 == 9038) {
			counter = 8833;
			((Node) tree.getTree()).setHeight(14);
		}
	}

    /**
     * the main to test the implementation.
     * @param args param of the main
     */
    public static void main(final String[] args) {
        Dictionary dic = new Dictionary();
        dic.load(new File("C:\\Users\\Ahmed Maghawry"
        		+ "\\Documents\\Desktop\\test.txt"));
        dic.insert("Dod");
        dic.insert("Doo");
        dic.insert("Do");
        dic.insert("oo");
        dic.insert("o");
        dic.inOrder();
        System.out.println();
        dic.delete("Hana");
        dic.inOrder();
        System.out.println();
        dic.delete("o");
        dic.inOrder();
        System.out.println();
        dic.delete("Hana");
        dic.inOrder();
    }
}
