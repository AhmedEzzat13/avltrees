package Imp;

import eg.edu.alexu.csd.filestructure.avl.INode;

public class Node implements INode {

    private Comparable value;
    private INode leftChild;
    private INode rightChild;
    private Node parent;
    private int height;

    public Node(Comparable value, INode leftChild, INode rightChild, Node parent) {
        this.value = value;
        this.leftChild = leftChild;
        this.rightChild = rightChild;
        this.parent = parent;
        this.height = 0;
    }

    @Override
    public INode getLeftChild() {
        // TODO Auto-generated method stub
        return leftChild;
    }

    @Override
    public INode getRightChild() {
        // TODO Auto-generated method stub
        return rightChild;
    }

    @Override
    public Comparable getValue() {
        // TODO Auto-generated method stub
        return value;
    }

    @Override
    public void setValue(Comparable value) {
        // TODO Auto-generated method stub
        this.value = value;
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(Node node) {
        parent = node;
    }

    public void setLeftChild(INode node) {
        leftChild = node;
    }

    public void setRightChild(INode node) {
        rightChild = node;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public boolean hasLeft() {
        if (leftChild != null)
            return true;
        else
            return false;
    }

    public boolean hasRight() {
        if (rightChild != null)
            return true;
        else
            return false;
    }
}
