package Imp;

import eg.edu.alexu.csd.filestructure.avl.IAVLTree;
import eg.edu.alexu.csd.filestructure.avl.INode;

/**
 * The imp of the AVL trees.
 * @author Ahmed Maghawry
 */
public class AVLTree implements IAVLTree {

	/**
	 * the reference which point to the root.
	 */
    private Node root = null;

    /**
     * Insert the given value using the key.
     * 
     * @param key
     *            the value to be inserted in the tree
     */
    public final void insert(final Comparable key) {
        Node newNode = new Node(key, null, null, null);
        insert(root, newNode);
    }

    /**
     * delete2 the key (if exists).
     * 
     * @param key
     *            the key of the node
     * @return true if node deleted, false if not exists
     */
    public final boolean delete(final Comparable key) {
        if (!search(key)) {
            return false;
        }
        delete2(key, root);
        return true;
    }

    /**
     * Search for a specific element using the key in the tree.
     * 
     * @param key
     *            the key of the node
     * @return true if the key exists, false otherwise
     */
    public final boolean search(final Comparable key) {
        Node node = root;
        while (node != null) {
            if (node.getValue().compareTo(key) == 0) {
                return true;
            } else if (key.compareTo(node.getValue()) < 0) {
                node = (Node) node.getLeftChild();
            } else {
                node = (Node) node.getRightChild();
            }
        }
        return false;
    }

    /**
     * Return the height of the AVL tree. This is the longest path from the root
     * to a leaf-node
     * 
     * @return tree height
     */
    public final int height() {
        return ((Node) getTree()).getHeight() + 1;
    }

    /**
     * Return the root of your AVL tree.
     * 
     * @return root of the AVL tree.
     */
    public final INode getTree() {
        return root;
    }
    
    private Node rotateDoubleRight(Node x) {
        if (x != null) {
            //x.setRightChild(rotateSingleLeft((Node)x.getRightChild()));
            x.setLeftChild(rotateSingleLeft((Node) x.getLeftChild()));
        }
        return rotateSingleRight(x);
    }

    private Node rotateSingleRight(Node x) {
        // TODO Auto-generated method stub
        if (x != null) {
            Node y = (Node) x.getLeftChild();
            x.setLeftChild(y.getRightChild());
            if (x.hasLeft()) {
                ((Node) x.getLeftChild()).setParent(x);
            }
            y.setRightChild(x);
            y.setParent(x.getParent());// *************
            x.setParent(y);
            if (y.getParent() != null) {
                if (y.getParent().getLeftChild() == x) {
                    y.getParent().setLeftChild(y);
                } else if (y.getParent().getRightChild() == x) {
                    y.getParent().setRightChild(y);
                }
            }
            x.setHeight(max((Node) x.getRightChild(), (Node) x.getLeftChild()) + 1);
            y.setHeight(max((Node) y.getRightChild(), (Node) y.getLeftChild()) + 1);
            return y;
        }
        return x;
    }

    private Node rotateDoubleLeft(Node x) {
        // TODO Auto-generated method stub
        if (x != null) {
            //x.setLeftChild(rotateSingleRight((Node)x.getLeftChild()));
            x.setRightChild(rotateSingleRight((Node) x.getRightChild()));
        }
    	//rotateSingleRight((Node)x.getRightChild());
        return rotateSingleLeft(x);
    }

    private Node rotateSingleLeft(Node x) {
        // TODO Auto-generated method stub
        if (x != null) {
            Node y = (Node) x.getRightChild();
            x.setRightChild(y.getLeftChild());
            if (x.hasRight()) {
                ((Node) x.getRightChild()).setParent(x);
            }
            y.setLeftChild(x);
            y.setParent(x.getParent());// *************
            x.setParent(y);
            if (y.getParent() != null) {
                if (y.getParent().getRightChild() == x) {
                    y.getParent().setRightChild(y);
                } else if (y.getParent().getLeftChild() == x) {
                    y.getParent().setLeftChild(y);
                }
            }
            x.setHeight(max((Node) x.getRightChild(), (Node) x.getLeftChild()) + 1);
            y.setHeight(max((Node) y.getRightChild(), (Node) y.getLeftChild()) + 1);
            return y;
        }
        return x;
    }

    private Node delete2(Comparable x, Node t) {
    	if (t == null) {
    		return null;
    	}
    	
    	int compareResult = x.compareTo(t.getValue());
    	if(compareResult < 0) {
    		t.setLeftChild(delete2(x, (Node)t.getLeftChild()));
    	} else if (compareResult > 0) {
    		t.setRightChild(delete2(x, (Node)t.getRightChild()));
    	} else if (t.getLeftChild() != null && t.getRightChild() != null) {
    		t.setValue(getMax((Node)t.getLeftChild()).getValue());
    		t.setRightChild(delete2(t.getValue(), (Node)t.getRightChild()));
    	} else {
    		t = (Node) ((t.getLeftChild() != null) ? t.getLeftChild() : t.getRightChild());
    	}
    	balanceEzzat(t);
    	return t;
    }
    
    private Node delete(Comparable key, Node node) {
        if (node == null) {
            return null;
        }
        if (key.compareTo(node.getValue()) < 0) {
            if (node.getLeftChild() == null) {
                return null;
            } else {
                node.setLeftChild(delete2(key, (Node) node.getLeftChild()));
                balanceEzzat(node);//***************************************************************
                node.setHeight(max((Node) node.getRightChild(), 
                		(Node) node.getLeftChild()) + 1);
                if (getHeight((Node) node.getRightChild())
                		- getHeight((Node) node.getLeftChild()) == 2) {
                    if (getHeight((Node) node.getRightChild().getLeftChild())
                    		> getHeight(
                            (Node) node.getRightChild().getRightChild())) {
                        balanceEzzat(node);
                        return node;
                    } else {
                        balanceEzzat(node);
                        return node;
                    }
                }
            }
        } else {
            if (key.compareTo(node.getValue()) > 0) {
                if (node.getRightChild() == null) {
                    return null;
                } else {
                    node.setRightChild(delete2(key,
                    		(Node) node.getRightChild()));
                    balanceEzzat(node);//**********************************************************
                    node.setHeight(max((Node) node.getLeftChild(),
                    		(Node) node.getRightChild()) + 1);
                    if (node.hasLeft() && node.hasRight()) {
                        if (getHeight((Node) node.getLeftChild())
                        		- getHeight((Node) node.getRightChild()) == 2) {
                            if (((Node) node.getLeftChild()).hasLeft()
                            		&& ((Node) node.getLeftChild())
                            		.hasRight()) {
                                if (getHeight((Node) node.getLeftChild()
                                		.getLeftChild()) > getHeight(
                                        (Node) node.getLeftChild()
                                        .getRightChild())) {
                                    balanceEzzat(node);
                                    return node;
                                } else {
                                    balanceEzzat(node);
                                    return node;
                                }
                            }
                        }
                    }
                }
            } else {
                if ((node.getRightChild() != null)
                		&& (node.getLeftChild() != null)) {
                    node.setValue((getMax((Node) node.getLeftChild()))
                    		.getValue());
                    node.setLeftChild(delete2(node.getValue(), (Node) node.getLeftChild()));
                    balanceEzzat(node);//***************************************************************
                } else {
                    if (node.getLeftChild() != null) {
                        return (Node) node.getLeftChild();
                    } else {
                        return (Node) node.getRightChild();
                    }
                }
            }
        }
        return node;
    }

    private int getHeight(Node node) {
        if (node != null)
            return node.getHeight();
        return -1;
    }

    private void insert(Node root, Node node) {
        if (root == null) {
            this.root = node;
        } else {
            if ((root.getValue()).compareTo(node.getValue()) < 0) {
                // Go to Right
                if (root.getRightChild() == null) {
                    root.setRightChild(node);
                    node.setParent(root);
                    balanceEzzat(root);
                } else {
                    insert((Node) root.getRightChild(), node);
                }
            } else if ((root.getValue()).compareTo(node.getValue()) > 0) {
                // Go to Left
                if (root.getLeftChild() == null) {
                    root.setLeftChild(node);
                    node.setParent(root);
                    balanceEzzat(root);
                } else {
                    insert((Node) root.getLeftChild(), node);
                }
            }
        }
    }

    private void balanceEzzat(Node parent) {
        if (parent != null) {
            int rightHeight = getNodeHeight((Node)parent.getRightChild());
            int leftHeight = getNodeHeight((Node)parent.getLeftChild());
            parent.setHeight(max((Node) parent.getRightChild(),
            		(Node) parent.getLeftChild()) + 1);
            if (leftHeight - rightHeight > 1) {
                // left left or left right
                if (getNodeHeight((Node)parent.getLeftChild().getLeftChild()) > getNodeHeight((Node)parent.getLeftChild().getRightChild())) {
                    // left left
                    parent = rotateSingleRight(parent);
                } else {
                    parent = rotateDoubleRight(parent);
                }
            } else if (leftHeight - rightHeight < -1) {
                // right right or right left
            	if (getNodeHeight((Node)parent.getRightChild().getRightChild()) > getNodeHeight((Node)parent.getRightChild().getLeftChild())) {
                    // right right
                    parent = rotateSingleLeft(parent);
                } else {
                    parent = rotateDoubleLeft(parent);
                }
            }
            if (parent.getParent() != null) {
                balanceEzzat(parent.getParent());
            } else {
                this.root = parent;
            }
        }
    }

    private int getNodeHeight(Node node) {
    	return (node != null) ? node.getHeight() : -1;
    }
    
    private int max(Node rightHeight, Node leftHeight) {
        if (rightHeight == null && leftHeight == null) {
            return -1;
        } else if (rightHeight == null) {
            return leftHeight.getHeight();
        } else if (leftHeight == null) {
            return rightHeight.getHeight();
        } else {
            if (rightHeight.getHeight() > leftHeight.getHeight())
                return rightHeight.getHeight();
            else
                return leftHeight.getHeight();
        }
    }

    private Node getMax(Node child) {
        if (child.hasRight()) {
            return getMax((Node) child.getRightChild());
        } else {
            return child;
        }
    }

    private void replace(Node node, Node child) {
        Comparable temp = node.getValue();
        node.setValue(child.getValue());
        child.setValue(temp);
    }

    private boolean isLeave(Node node) {
        if (node.hasLeft() || node.hasRight())
            return false;
        return true;
    }

    public void inorder() {
        inorder(root);
    }

    private void inorder(Node r) {
        if (r != null) {
            inorder((Node) r.getLeftChild());
            System.out.print(r.getValue() + " ");
            inorder((Node) r.getRightChild());
        }
    }

    public static void main(String[] args) {
        // Scanner x = new Scanner(System.in);
        AVLTree tree = new AVLTree();
        tree.insert(7);
        tree.insert(10);
        tree.insert(9);
        tree.insert(23);
        tree.insert(2);
        tree.insert(5);
        tree.insert(1);
        tree.insert(22);
        tree.insert(0);
        System.out.println(tree.getTree().getValue());
        tree.inorder();
        System.out.println();
        tree.delete(9);
        tree.delete(1);
        tree.delete(23);
        tree.inorder();
        System.out.println();
        System.out.println(tree.height());
        System.out.println(tree.search(20));
        System.out.println(tree.search(23));
        System.out.println(tree.search(5));
    }
}
