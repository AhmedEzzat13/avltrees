package eg.edu.alexu.csd.filestructure.test.avl;

import Imp.AVLTree;
import Imp.Dictionary;
import eg.edu.alexu.csd.filestructure.avl.*;

public class TestRunner {

	public static IAVLTree getImplementationInstance() {
		return new AVLTree();
	}

	public static IDictionary getDicImplementationInstance() {
		return new Dictionary();
	}

}
